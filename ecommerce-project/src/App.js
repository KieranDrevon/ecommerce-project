import SearchAppBar from './components/Search';
import ProductsList from './components/ProductsList';
import CircularIndeterminate from './alerts/Loading';
import React, { useState, useEffect } from 'react'
import Product from './components/Product';
import axios from 'axios'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'

function App() {

  const [products, setProducts] = useState([])
  const [loading, setLoading] = useState(false)
  const [searchField, setSearchField] = useState('')


  useEffect(() => {
    setLoading(true)
    axios
      .get('https://fakestoreapi.com/products')
          .then((response) =>
            setProducts(response))
          .catch((e) => console.log(e))
          .finally(() => setLoading(false))
          
  }, [])

  const handleSearchChange = (event) => {
    setSearchField(event.target.value)
  }

  if (loading) {
    return (
      <>
      <SearchAppBar />
      <CircularIndeterminate />
      </>
    )
  }

  return (
    <BrowserRouter>
        <SearchAppBar searchField={searchField} handleSearch={handleSearchChange} />
        <div className="App">
          <Routes>
            <Route path='/' element={<Navigate to='/products' />} />
              <Route path='products' element={<ProductsList products={products} searchField={searchField} />} />
              <Route path ='/products/:productId' element={<Product />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
