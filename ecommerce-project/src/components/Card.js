import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Divider } from '@mui/material';
import { Link } from 'react-router-dom';

export default function MediaCard({ product }) {
  return (
    <Card sx={{ maxWidth: "25%", alignItems: 'stretch', boxShadow: '0 4px 8px rgba(0,0,0,0.2)'}}>
      <CardMedia
        sx={{ height: "194", objectFit: 'cover', width: '100%' }}
        component={'img'}
        image={product.image}
        title="product image"
      />
      <CardContent>
        {/* <Typography gutterBottom variant="h5" component="div" sx={{ fontWeight: 'bold'}}>
        {product.category[0].toUpperCase() + product.category.slice(1)}
        </Typography> */}
        <Typography variant='h5' sx={{fontWeight: 'bold'}}>
            {product.title}
        </Typography>
        <Divider light />
        <br></br>
        <Typography variant="body2" color="text.secondary">
          {product.description}
        </Typography>
        <br></br>
        <Typography variant='body3' sx={{fontWeight: 'bold'}}>
            ${product.price}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" variant='outlined'>Add to Cart</Button>
        <Link to={`${product.id}`} style={{ textDecoration: 'none' }}>
            <Button size="small" variant='outlined'>Details</Button>
        </Link>
      </CardActions>
    </Card>
  );
}