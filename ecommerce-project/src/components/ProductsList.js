import * as React from 'react';
import Masonry from '@mui/lab/Masonry';
import MediaCard from './Card';
import { Grid } from '@mui/material';

// const heights = [150, 30, 90, 70, 110, 150, 130, 80, 50, 90, 100, 150, 30, 50, 80];


export default function ProductsList({ products, searchField }) {
   
  return (
    <Grid sx={{ width: 'auto', minHeight: 393, marginTop: '15px' }}>
      <Masonry columns={4} spacing={2}>
            {/* {products.data?.map(product => (
                <MediaCard product={product} key={product.id}/>
            ))
        } */}
        {products.data?.filter(product => {if (searchField === "") {
            return product
        } else if (product.title.toLowerCase().includes(searchField.toLowerCase())) {
            return product
        }
        })
        .map(product => 
        <MediaCard product={product} key={product.id}/>)}
      </Masonry>
    </Grid>
  );
}