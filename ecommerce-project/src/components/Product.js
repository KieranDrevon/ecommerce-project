import * as React from 'react';
import '../styles/product.css'
import {useParams} from 'react-router-dom'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { Button, Typography } from '@mui/material';
import image from '../images/code.png'
import Divider from '@mui/material/Divider';

export default function Product({ }) {
    let {productId} = useParams();
    const [product, setProduct] = useState({})

    useEffect( () => { axios
        .get(`https://fakestoreapi.com/products/${productId}`)
        .then(res=>setProduct(res.data))
        console.log(product.image)
        
    }, [])




  return (
        <main className="container">
            <div className="left-column">
                <img src={product.image} alt="" />
            </div>
        
            <div className="right-column">
            
                
                <div className="product-description">
                <span style={{fontSize: 'large'}}>{product.category}</span>
                <Divider light />
                <h1>{product.title}</h1>
                <p>{product.description}</p>
                </div>
                <div className="product-price">
                <Typography variant='body3' sx={{fontWeight: 'bold'}}>
                ${product.price}
                </Typography>
                <br></br>
                <br></br>
                <Button size="small" variant='outlined'>Add to Cart</Button>
                </div>
            </div>
        </main>
  );
}