import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

export default function CircularIndeterminate() {
  return (
    <Box sx={{ display: 'flex' }}>
        <div>
            <h1>Loading Products...</h1>
                <CircularProgress />

        </div>
    </Box>
  );
}